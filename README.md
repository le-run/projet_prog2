
# Projet Programmation 2 : Tower Defense

## Warning

Placing towers during the wave is still experimental and can throw harmless
exceptions.

## Building

How to compile:

```bash
make
```

How to run: (also compiles)

```bash
make run
```

How to package in a (kinda executable) .jar:

```bash
make package
```
